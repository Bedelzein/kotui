package com.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.bedelzein.core.android.toast
import kz.bedelzein.kotui.Air
import kz.bedelzein.kotui.ClickListener
import kz.bedelzein.kotui.ItemsManager
import kz.bedelzein.kotui.KotuiActivity
import kz.bedelzein.kotui.Linear
import kz.bedelzein.kotui.ScreenUi
import kz.bedelzein.kotui.Text
import kz.bedelzein.kotui.UiButton
import kz.bedelzein.kotui.UiConstraint
import kz.bedelzein.kotui.UiList
import kz.bedelzein.kotui.Water
import kz.bedelzein.kotui.get
import kz.bedelzein.kotui.invoke

class MainActivity : AppCompatActivity(), KotuiActivity {

    lateinit var uiList: ItemsManager<MockItem>

    @Suppress("unused")
    private val screenUi: ScreenUi<*> by ScreenUi.Delegate(this, UiConstraint.Itself::class) {
        uiList = UiList<MockItem>()[Linear] {
            uiItem<MockItem.Number>(Water) { provider ->
                UiButton()[
                    Text(provider.defaultItem.value.toString()),
                    ClickListener {
                        val item = provider.getActualItem() ?: return@ClickListener
                        toast("${item.value} clicked!")
                    }
                ]
            }
            uiItem<MockItem.Text>(Water) { provider ->
                UiButton()[
                    Text(provider.defaultItem.value),
                    ClickListener {
                        val item = provider.getActualItem() ?: return@ClickListener
                        toast("${item.value} clicked!")
                    }
                ]
            }
        }(Air)
    }

    @Suppress("MagicNumber")
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        uiList.submitItems((1..10).map { MockItem.Number(it) } + MockItem.Text("11"))
    }

    sealed class MockItem : UiList.Item {
        class Number(val value: Int) : MockItem() {
            override fun isSameAs(item: UiList.Item): Boolean =
                item is Number && item.value == value

            override fun areContentsSameWith(item: UiList.Item): Boolean =
                item is Number && item.value == value
        }

        class Text(val value: String) : MockItem() {
            override fun isSameAs(item: UiList.Item): Boolean =
                item is Text && item.value == value

            override fun areContentsSameWith(item: UiList.Item): Boolean =
                item is Text && item.value == value
        }
    }
}