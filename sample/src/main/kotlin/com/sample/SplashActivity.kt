package com.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.bedelzein.core.android.navigateTo

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigateTo<MainActivity>()
        finish()
    }
}