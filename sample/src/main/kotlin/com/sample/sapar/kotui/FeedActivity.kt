package com.sample.sapar.kotui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kz.bedelzein.core.android.navigateTo
import kz.bedelzein.kotui.Click
import kz.bedelzein.kotui.KotuiActivity
import kz.bedelzein.kotui.ScreenUi
import kz.bedelzein.kotui.UiButton
import kz.bedelzein.kotui.UiConstraint
import kz.bedelzein.kotui.earth
import kz.bedelzein.kotui.get
import kz.bedelzein.kotui.invoke
import com.sample.sapar.standard.FeedActivity as StandardFeedActivity

class FeedActivity : AppCompatActivity(), KotuiActivity {

    private val catalogueClicked = MutableSharedFlow<Unit>()

    @Suppress("unused")
    private val screenUi: ScreenUi<*> by ScreenUi.Delegate(this, UiConstraint.Itself::class) {
        lateinit var favouritesButton: UiButton
        lateinit var mapButton: UiButton
        lateinit var settingsButton: UiButton

        val catalogueButton = UiButton()[Click(catalogueClicked)] {
            startTo { parentStart }
            endTo { favouritesButton.start }
            bottomTo { parentBottom + 8.dip }
        }(70.dip.earth)

        favouritesButton = UiButton().invoke {
            startTo { catalogueButton.end }
            topTo { catalogueButton.top }
            endTo { mapButton.start }
            bottomTo { catalogueButton.bottom }
        }(70.dip.earth)

        mapButton = UiButton().invoke {
            startTo { favouritesButton.end }
            topTo { catalogueButton.top }
            endTo { settingsButton.start }
            bottomTo { catalogueButton.bottom }
        }(70.dip.earth)

        settingsButton = UiButton().invoke {
            startTo { mapButton.end }
            topTo { catalogueButton.top }
            endTo { parentEnd }
            bottomTo { catalogueButton.bottom }
        }(70.dip.earth)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            catalogueClicked.collect {
                navigateTo<StandardFeedActivity>()
            }
        }
    }
}