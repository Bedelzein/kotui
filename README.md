# Kotui

Kotui is an android library of declarative UI, inspired and influenced by the [Anko library](https://github.com/Kotlin/anko).  
It was decided to continue that kind of UI implementation, when [Anko was deprecated](https://github.com/Kotlin/anko/blob/master/GOODBYE.md).  
The first try of implementation was [Kotlify](https://github.com/Brotandos/Kotlify), but continuation of this project required a lot of resources and time.  
Also it was dependent on RxJava library. That library was created in order to make declarative UI library growth more convenient and independent from async frameworks.

## Submodules
Repo uses git submodules. After first cloning update all submodule repositories with next command:
```
git submodule update --init --recursive
```

Be aware after cloning. Each submodule should be checked out on `master` branch.

## Getting started

### ConstraintLayout
<table>
<tr><td> Source </td> <td> Result </td></tr>
<tr>
<td>

```kotlin
class SampleKotuiActivity : AppCompatActivity(), KotuiActivity {

    private val screenUi: ScreenUi<*> by ScreenUi.Delegate(
        this,
        UiConstraint.Itself::class
    ) {
    
        UiConnectable[Text("Hello, World!")] {
            id = Id(R.id.hello_world_text_view)
            startTo { parentStart }
            topTo { parentTop }
            endTo { parentEnd }
            bottomTo { parentBottom }
        }(Earth)
        
        lateinit var bButton: UiButton
        lateinit var cButton: UiButton
        lateinit var dButton: UiButton
        
        val aButton = UiButton()[Text.Value("a")] {
            startTo { parentStart }
            endTo { bButton.start }
            bottomTo { parentBottom + 8.dip }
        }(70.dip.earth)
        
        bButton = UiButton()[Text.Value("b")] {
            startTo { aButton.end }
            topTo { aButton.top }
            endTo { cButton.start }
            bottomTo { aButton.bottom }
        }(70.dip.earth)
        
        cButton = UiButton()[Text.Value("c")] {
            startTo { bButton.end }
            topTo { aButton.top }
            endTo { dButton.start }
            bottomTo { aButton.bottom }
        }(70.dip.earth)
        
        dButton = UiButton()[Text.Value("d")] {
            startTo { cButton.end }
            topTo { aButton.top }
            endTo { parentEnd }
            bottomTo { aButton.bottom }
        }(70.dip.earth)
    }
}
```
</td>
<td><img width="300" alt="Screenshot" src="docs/images/constraint_layout_example.png"></td>
</tr>
</table>

`UiConnectable` - wrapper of `View`.

`UiComposite` - wrapper of `ViewGroup`

`Earth` is "aggregation state" of view and means that view has `wrapContent` width and `wrapContent` height. `70.dip.earth` means that view will have custom `70dp` width and `wrapContent` height

### Aggregation state

For writing ui markup faster, shorter and more comfortable there're few keywords of view's layout condition (width and height):

|                 | Width        | Height       |
|:--------------- |:-------------|:-------------|
| Earth           | WRAP_CONTENT | WRAP_CONTENT |
| Water           | MATCH_PARENT | WRAP_CONTENT |
| Air             | MATCH_PARENT | MATCH_PARENT |
| Fire            | WRAP_CONTENT | MATCH_PARENT |
| Custom x Earth  | Custom       | WRAP_CONTENT |
| Earth x Custom  | WRAP_CONTENT | Custom       |
| Custom x Water  | MATCH_PARENT | Custom       |
| Custom x Fire   | Custom       | MATCH_PARENT |
| Custom x Custom | Custom       | Custom       |

## TODO
* Optimize [cache system](https://appmattus.medium.com/caching-made-simple-on-android-d6e024e3726b) for lazy storing ids of view in order to use it inside UiConstraint
* Make test for ScreenUi.Delegate if everything works fine about
  lifecycle issues
* Configure CI/CD for running UI tests
  - [Stackoverflow issue](https://stackoverflow.com/questions/50554918/gitlab-ci-ui-tests)
  - Gitlab official post "[Setting up Gitlab CI for Android projects](https://about.gitlab.com/blog/2018/10/24/setting-up-gitlab-ci-for-android-projects/)"
  - [Another gitlab's official post](https://about.gitlab.com/blog/2017/11/20/working-with-yaml-gitlab-ci-android/)
  - [GitLab issue](https://gitlab.com/gitlab-com/blog-posts/-/issues/28)
  - [Using Gitlab to set up a CI/CD workflow for an Android App from scratch](https://blog.mi.hdm-stuttgart.de/index.php/2020/02/24/using-gitlab-to-set-up-a-ci-cd-workflow-for-an-android-app-from-scratch/)
  - [Example of GitLab CI](https://gitlab.com/illuzor/cube-timer/-/blob/master/.gitlab-ci.yml)
  - [Another example of GitLab CI](https://gitlab.com/HerHde/instrumentalized/-/blob/b4ff04c74ecc7311fc7188395eff17fa6c0bfbe7/.gitlab-ci.yml)
  - [More another example of GitLab CI configuration in GitHub gist](https://gist.github.com/illuzor/988385c493d3f7ed7193a6e3ce001a68)

## Thanks to
* [Статья, на основе которого будут писаться биндинги](https://habr.com/ru/company/mobileup/blog/342850/)
* [Explanation of RecyclerView](https://ziginsider.github.io/RecyclerView/)
* [ContourLayout](https://github.com/cashapp/contour)
* [SaveState ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel-savedstate)
* [DSLint - A lightweight Android linter for Kotlin DSL aimed to solve the problem of verifying mandatory DSL attributes at compile time.](https://github.com/ironSource/dslint)
* [Vuetify library for inspiration](https://vuetifyjs.com)
* Code generation posts:
  - [Кодогенерация в Uber](https://habr.com/ru/company/e-Legion/blog/413603/)
  - [Kotlinpoet](https://square.github.io/kotlinpoet/)
  - [Idiomatic Kotlin: Annotation Processor and Code Generation](https://medium.com/tompee/kotlin-annotation-processor-and-code-generation-58bd7d0d333b)
* [Post about making progress drawable inside button](https://proandroiddev.com/replace-progressdialog-with-a-progress-button-in-your-app-14ed1d50b44)
* Permissions handling library:
  - [Tbruyelle's RxPermissions](https://github.com/tbruyelle/RxPermissions)
  - [Vanniktech's RxPermission](https://github.com/vanniktech/RxPermission)
  - [TedPermission](https://github.com/ParkSangGwon/TedPermission)
* [DrawableToolbox](https://github.com/duanhong169/DrawableToolbox) - lib helps to create drawables programmatically
* [Paris](https://github.com/airbnb/paris)
* [Using GSON with Kotlin’s Non-Null Types](https://medium.com/swlh/using-gson-with-kotlins-non-null-types-468b1c66bd8b)