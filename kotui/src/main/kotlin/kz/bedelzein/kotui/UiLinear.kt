package kz.bedelzein.kotui

import android.widget.LinearLayout

class UiLinear : UiComposite<LinearLayout, LinearLayout.LayoutParams>()

abstract class Orientation<V : LinearLayout> : UiElement.Attr<V> {
    object Vertical : Orientation<LinearLayout>() {
        override suspend fun setValue(uiElement: LinearLayout) {
            uiElement.orientation = LinearLayout.VERTICAL
        }
    }

    object Horizontal : Orientation<LinearLayout>() {
        override suspend fun setValue(uiElement: LinearLayout) {
            uiElement.orientation = LinearLayout.HORIZONTAL
        }
    }
}