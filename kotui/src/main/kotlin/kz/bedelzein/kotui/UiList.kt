package kz.bedelzein.kotui

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.coroutines.runBlocking
import kz.bedelzein.core.kotlin.claim
import kz.bedelzein.core.kotlin.claimNotNull
import kotlin.reflect.KClass

open class UiList<T : UiList.Item> : UiComposite<RecyclerView, RecyclerView.LayoutParams>(),
    ItemsManager<T> {

    private val itemCallback = object : DiffUtil.ItemCallback<T>() {
        override fun areItemsTheSame(oldItem: T, newItem: T): Boolean =
            oldItem.isSameAs(newItem)

        override fun areContentsTheSame(oldItem: T, newItem: T): Boolean =
            oldItem.areContentsSameWith(newItem)
    }

    private val adapter: ListAdapter<T, ViewHolder> by lazy {
        object : ListAdapter<T, ViewHolder>(itemCallback) {
            override fun onCreateViewHolder(
                parent: ViewGroup,
                position: Int
            ): ViewHolder {
                val item = getItem(position)
                val (width, height, uiFactory) = claimNotNull(itemsMarkupMap[item::class])
                val provider = Item.Provider(item, adapter)
                val uiConnectable = uiFactory.invoke(provider)
                val itemView = runBlocking { uiConnectable.build(parent.context) }
                itemView.layoutParams = ViewGroup.LayoutParams(width, height)
                ViewHolder(itemView).let {
                    provider.viewHolder = it
                    return it
                }
            }
            override fun onBindViewHolder(holder: ViewHolder, position: Int) = Unit
            override fun getItemViewType(position: Int) = position
        }
    }

    @PublishedApi
    internal val itemsMarkupMap = mutableMapOf<KClass<*>, Item.Ui<T, T>>()

    override suspend fun build(context: Context): RecyclerView =
        super.build(context).also { it.adapter = adapter }

    @SuppressLint("MissingSuperCall")
    override fun <T : UiConnectable<*>> addContinuation(
        nextContinuation: T,
        width: Int,
        height: Int,
        init: (RecyclerView.LayoutParams.() -> Unit)?
    ): T = throw IllegalArgumentException("Use UiList#uiItem function to add continuation view")

    override fun submitItems(items: List<T>) = adapter.submitList(items)

    override fun getCurrentItems(): List<T> = adapter.currentList

    inline fun <reified E : T> uiItem(
        aggregation: AggregationState = Earth,
        noinline uiConnectableForItem: (Item.Provider<T, E>) -> UiConnectable<*>
    ) {
        val (width, height) = aggregation.getParams()
        uiItem(width, height, uiConnectableForItem)
    }

    inline fun <reified E : T> uiItem(
        width: Int,
        height: Int,
        noinline uiConnectableForItem: (Item.Provider<T, E>) -> UiConnectable<*>
    ) {
        val key = E::class
        require(itemsMarkupMap[key] == null) { "You should register item murkup only once" }
        itemsMarkupMap[key] = Item.Ui(width, height) { uiConnectableForItem(claim(it)) }
    }

    interface Item {
        fun isSameAs(item: Item): Boolean
        fun areContentsSameWith(item: Item): Boolean

        data class Ui<T : Item, E : T>(
            val width: Int,
            val height: Int,
            val factory: (Provider<T, E>) -> UiConnectable<*>
        )

        class Provider<T : Item, E : T>(
            val defaultItem: E,
            @PublishedApi internal val adapter: ListAdapter<T, ViewHolder>
        ) {
            @PublishedApi
            internal var viewHolder: ViewHolder? = null

            fun getActualItem(): E? {
                val position = viewHolder?.adapterPosition
                    ?.takeIf { it != RecyclerView.NO_POSITION }
                    ?: return null
                return adapter.currentList[position] as E
            }
        }
    }

    class ViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView)
}

interface ItemsManager<E : UiList.Item> {
    fun submitItems(items: List<E>)
    fun getCurrentItems(): List<E>
}

sealed class Layout : UiElement.Attr<RecyclerView> {
    abstract fun getImplementation(context: Context): RecyclerView.LayoutManager
}

object Linear : Layout() {
    override fun getImplementation(context: Context) = LinearLayoutManager(context)
    override suspend fun setValue(uiElement: RecyclerView) =
        uiElement.setLayoutManager(getImplementation(uiElement.context))
}

class Grid(private val spanCount: Int) : Layout() {
    override fun getImplementation(
        context: Context
    ) = GridLayoutManager(context, spanCount)

    override suspend fun setValue(uiElement: RecyclerView) =
        uiElement.setLayoutManager(getImplementation(uiElement.context))
}

class Staggered(private val spanCount: Int, val isVertical: Boolean) : Layout() {
    override fun getImplementation(
        context: Context
    ) = StaggeredGridLayoutManager(
        spanCount,
        if (isVertical) RecyclerView.VERTICAL else RecyclerView.HORIZONTAL
    )

    override suspend fun setValue(uiElement: RecyclerView) =
        uiElement.setLayoutManager(getImplementation(uiElement.context))
}