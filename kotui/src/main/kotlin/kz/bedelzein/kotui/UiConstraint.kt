package kz.bedelzein.kotui

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import kz.bedelzein.core.kotlin.claimNotNull
import kotlin.reflect.KClass

private const val ERROR_SETTING_CONSTRAINT_TO_LAYOUT_ITSELF =
    "You can set constraints only for ConstraintLayout's direct children"

private typealias FatherContinuation2Mother = MutableMap<ConstraintSide, () -> ConstraintDirection>

private typealias FatherSource2Relationship = MutableMap<UiConnectable<*>, FatherContinuation2Mother>

@Suppress("TooManyFunctions", "PARAMETER_NAME_CHANGED_ON_OVERRIDE")
open class UiConstraint<V : ConstraintLayout, LP : ConstraintLayout.LayoutParams> :
    UiComposite<V, LP>() {

    private val constraints: FatherSource2Relationship = mutableMapOf()

    val parentStart = ConstraintSide.Start.getDirection()
    val parentTop = ConstraintSide.Top.getDirection()
    val parentEnd = ConstraintSide.End.getDirection()
    val parentBottom = ConstraintSide.Bottom.getDirection()

    val UiConnectable<*>.start get() = ConstraintSide.Start.getDirection(this)
    val UiConnectable<*>.top get() = ConstraintSide.Top.getDirection(this)
    val UiConnectable<*>.end get() = ConstraintSide.End.getDirection(this)
    val UiConnectable<*>.bottom get() = ConstraintSide.Bottom.getDirection(this)

    override fun <T : UiConnectable<*>> addContinuation(
        nextContinuation: T,
        width: Int,
        height: Int,
        init: (LP.() -> Unit)?
    ): T {
        if (nextContinuation.isIdUndefined) identify(nextContinuation)
        return super.addContinuation(nextContinuation, width, height, init)
    }

    override fun onContinuationsBuilt(constraintLayout: V) {
        super.onContinuationsBuilt(constraintLayout)
        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintLayout)
        for ((sourceConnectable, sides2Directions) in constraints) {
            sides2Directions.forEach { (sourceSide, constraintDirectionGetter) ->
                val constraintDirection = constraintDirectionGetter.invoke()
                constraintDirection.accept(constraintSet, sourceConnectable, sourceSide)
            }
        }
        constraintSet.applyTo(constraintLayout)
    }

    fun UiConnectable<*>.startTo(targetGetter: () -> HorizontalDirection) =
        addConstraint(this, ConstraintSide.Start, targetGetter)

    fun UiConnectable<*>.topTo(targetGetter: () -> VerticalDirection) =
        addConstraint(this, ConstraintSide.Top, targetGetter)

    fun UiConnectable<*>.endTo(targetGetter: () -> HorizontalDirection) =
        addConstraint(this, ConstraintSide.End, targetGetter)

    fun UiConnectable<*>.bottomTo(targetGetter: () -> VerticalDirection) =
        addConstraint(this, ConstraintSide.Bottom, targetGetter)

    operator fun HorizontalDirection.plus(margin: Int): HorizontalDirection = clone(margin)

    operator fun HorizontalDirection.minus(margin: Int): HorizontalDirection = clone(margin)

    operator fun VerticalDirection.plus(margin: Int): VerticalDirection = clone(margin)

    operator fun VerticalDirection.minus(margin: Int): VerticalDirection = clone(margin)

    private fun identify(continuation: UiConnectable<*>) {
        val container = this@UiConstraint
        val continuationPath = continuation.getPath(
            claimNotNull(container.path),
            container.getNextContinuationPosition()
        )
        val idValue = LazyIds[continuationPath] ?: View.generateViewId()
        val id = Id.Defined(idValue)
        continuation.id = id
        LazyIds[continuationPath] = id.value
    }

    private fun addConstraint(
        source: UiConnectable<*>,
        sourceSide: ConstraintSide,
        constraintDirectionGetter: () -> ConstraintDirection
    ) {
        require(source !== this) { ERROR_SETTING_CONSTRAINT_TO_LAYOUT_ITSELF }
        val constraintDirection = constraintDirectionGetter()
        if (constraintDirection.target is ConstraintDirection.Target.Continuation) {
            require(source !== constraintDirection.target.continuation) {
                "Constraint direction shouldn't be set for itself"
            }
        }
        constraints[source]?.let {
            it[sourceSide] = constraintDirectionGetter
            return
        }
        constraints[source] = mutableMapOf(sourceSide to constraintDirectionGetter)
    }

    class Itself : UiConstraint<ConstraintLayout, ConstraintLayout.LayoutParams>() {
        companion object {
            operator fun invoke(block: UiConstraint<*, *>.() -> Unit) = Itself().invoke(block)
        }
    }
}

/**
 * If [ConstraintSidePole.POSITIVE], then side is [ConstraintSide.Start] or [ConstraintSide.Top]
 * If [ConstraintSidePole.NEGATIVE], then side is [ConstraintSide.End] or [ConstraintSide.Bottom]
 */

enum class ConstraintSidePole {
    POSITIVE {
        override fun <T : ConstraintDirection> getSide(
            directionClass: KClass<T>
        ): ConstraintSide = if (isHorizontal(directionClass)) {
            ConstraintSide.Start
        } else {
            ConstraintSide.Top
        }
    },
    NEGATIVE {
        override fun <T : ConstraintDirection> getSide(
            directionClass: KClass<T>
        ): ConstraintSide = if (isHorizontal(directionClass)) {
            ConstraintSide.End
        } else {
            ConstraintSide.Bottom
        }
    };

    abstract fun <T : ConstraintDirection> getSide(directionClass: KClass<T>): ConstraintSide

    internal fun getHorizontalDirection(
        continuation: UiConnectable<*>?
    ) = HorizontalDirection(this, ConstraintDirection.Target(continuation))

    internal fun getVerticalDirection(
        continuation: UiConnectable<*>?
    ) = VerticalDirection(this, ConstraintDirection.Target(continuation))

    protected fun <T : ConstraintDirection> isHorizontal(
        directionClass: KClass<T>
    ) = directionClass == HorizontalDirection::class
}

sealed class ConstraintSide(val value: Int) {

    private val constraintSourceContainer = null

    abstract fun getDirection(source: UiConnectable<*>? = constraintSourceContainer): ConstraintDirection

    object Start : ConstraintSide(ConstraintSet.START) {
        override fun getDirection(source: UiConnectable<*>?): HorizontalDirection =
            ConstraintSidePole.POSITIVE.getHorizontalDirection(source)
    }

    object Top : ConstraintSide(ConstraintSet.TOP) {
        override fun getDirection(source: UiConnectable<*>?): VerticalDirection =
            ConstraintSidePole.POSITIVE.getVerticalDirection(source)
    }

    object End : ConstraintSide(ConstraintSet.END) {
        override fun getDirection(source: UiConnectable<*>?): HorizontalDirection =
            ConstraintSidePole.NEGATIVE.getHorizontalDirection(source)
    }

    object Bottom : ConstraintSide(ConstraintSet.BOTTOM) {
        override fun getDirection(source: UiConnectable<*>?): VerticalDirection =
            ConstraintSidePole.NEGATIVE.getVerticalDirection(source)
    }
}

abstract class ConstraintDirection(
    val target: Target = Target.Container,
    private val side: ConstraintSide,
    private val nullableMargin: Int? = null
) {
    abstract fun clone(margin: Int): ConstraintDirection

    fun accept(
        constraintSet: ConstraintSet,
        sourceConnectable: UiConnectable<*>,
        sourceSide: ConstraintSide
    ) {
        nullableMargin?.let { margin ->
            constraintSet.connect(
                sourceConnectable.id.requireDefinedValue(),
                sourceSide.value,
                target.id,
                side.value,
                margin
            )
        } ?: constraintSet.connect(
            sourceConnectable.id.requireDefinedValue(),
            sourceSide.value,
            target.id,
            side.value
        )
    }

    protected fun requireMarginNotSetBefore() {
        require(nullableMargin == null) { "You should set margin only once due to SSOT" }
    }

    sealed class Target {

        companion object {
            operator fun invoke(continuation: UiConnectable<*>?) =
                continuation?.let { Continuation(it) } ?: Container
        }

        abstract val id: Int

        object Container : Target() {
            override val id: Int = ConstraintSet.PARENT_ID
        }

        data class Continuation(
            val continuation: UiConnectable<*>
        ) : Target() {
            override val id: Int = continuation.id.requireDefinedValue()
        }
    }
}

class HorizontalDirection(
    private val pole: ConstraintSidePole,
    target: Target = Target.Container,
    margin: Int? = null
) : ConstraintDirection(target, pole.side, margin) {

    companion object {
        private val ConstraintSidePole.side get() = getSide(HorizontalDirection::class)
    }

    override fun clone(margin: Int): HorizontalDirection {
        requireMarginNotSetBefore()
        return HorizontalDirection(pole, target, margin)
    }
}

class VerticalDirection(
    private val pole: ConstraintSidePole,
    target: Target = Target.Container,
    margin: Int? = null
) : ConstraintDirection(target, pole.side, margin) {

    companion object {
        private val ConstraintSidePole.side get() = getSide(VerticalDirection::class)
    }

    override fun clone(margin: Int): VerticalDirection {
        requireMarginNotSetBefore()
        return VerticalDirection(pole, target, margin)
    }
}