package kz.bedelzein.kotui

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams
import androidx.annotation.CallSuper
import kz.bedelzein.core.kotlin.claimNotNull
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf

open class UiComposite<V : ViewGroup, LP : LayoutParams> @PublishedApi internal constructor() :
    UiConnectable<V>() {

    /**
     * Wrapping child [View]s for [ViewGroup]
     * */
    private val continuations = mutableListOf<UiConnectable<*>>()

    override suspend fun build(context: Context): V {
        val view = super.build(context)
        buildContinuations(view)
        onContinuationsBuilt(view)
        return view
    }

    /**
     * Wrapping [ViewGroup.LayoutParams]
     * Required for adding [UiConnectable] to [UiComposite] as a child
     * */
    operator fun <T : UiConnectable<*>> T.invoke(
        width: Int,
        height: Int,
        init: (LP.() -> Unit)? = null
    ): T = addContinuation(this, width, height, init)

    operator fun <T : UiConnectable<*>> T.invoke(
        aggregation: AggregationState,
        init: (LP.() -> Unit)? = null
    ): T {
        val (width, height) = aggregation.getParams()
        return invoke(width, height, init)
    }

    fun getNextContinuationPosition() = continuations.size

    @CallSuper
    protected open fun <T : UiConnectable<*>> addContinuation(
        nextContinuation: T,
        width: Int,
        height: Int,
        init: (LP.() -> Unit)? = null
    ): T {
        require(nextContinuation !== this) { "You cannot create layout params from itself" }

        require(continuations.none { it === nextContinuation }) {
            "You shouldn't double the same view several times"
        }

        val continuationName = nextContinuation.getPath(
            claimNotNull(path),
            getNextContinuationPosition()
        )

        continuations += nextContinuation

        if (nextContinuation.isUiComposite()) {
            ensurePathChainSetting(nextContinuation, continuationName)
        }

        require(nextContinuation.placing == Placing.NotSet) {
            "Layout params for this connectable is already set before. View must have only single parent"
        }

        require(continuations.any { it === nextContinuation }) {
            "$this isn't parent of $nextContinuation. Check dsl hierarchy for validity"
        }

        val layoutParams = Hardcode.createLayoutParams(
            this@UiComposite,
            width,
            height
        ) as LP

        nextContinuation.placing = Placing.Continuation(layoutParams, init)

        return nextContinuation
    }

    /**
     * Initially it was required for setting constraints of [UiConnectable] inside [UiConstraint]
     * */
    @CallSuper
    protected open fun onContinuationsBuilt(view: V) = Unit

    private suspend fun buildContinuations(viewGroup: ViewGroup) {
        val context = viewGroup.context
        continuations.forEach { uiConnectable ->
            val continuationView = uiConnectable.build(context)
            viewGroup.addView(continuationView)
        }
    }

    /**
     * Created in purposes of setting path for [UiComposite] inside [UiComposite.addContinuation]
     * */
    private fun UiConnectable<*>.isUiComposite() = this::class.isSubclassOf(UiComposite::class)

    private fun ensurePathChainSetting(
        uiConnectable: UiConnectable<*>,
        path: String
    ) {
        val id = Id.Defined(View.generateViewId())
        uiConnectable.id = id
        LazyIds[path] = id.value
    }

    companion object {
        inline fun <reified VG : ViewGroup, LP : LayoutParams> custom() =
            UiComposite<VG, LP>().also { it.viewClass = VG::class }

        inline operator fun <reified VG : ViewGroup, reified LP : LayoutParams> get(
            viewClass: KClass<VG>,
            lpClass: KClass<LP>,
            vararg attrs: UiElement.Attr<VG>
        ): UiComposite<VG, LP> {
            val uiComposite = UiComposite<VG, LP>()
            uiComposite.viewClass = viewClass
            uiComposite.setAttrs(attrs.toSet())
            return uiComposite
        }
    }
}