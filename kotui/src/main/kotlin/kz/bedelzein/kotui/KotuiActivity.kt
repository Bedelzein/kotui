package kz.bedelzein.kotui

import android.app.Activity
import android.util.TypedValue
import kz.bedelzein.core.kotlin.claim
import kotlin.math.roundToInt

interface KotuiActivity {

    private val instance: Activity get() = claim(this)

    val Int.dip: Int
        get() = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this.toFloat(),
            instance.resources.displayMetrics
        ).roundToInt()
}