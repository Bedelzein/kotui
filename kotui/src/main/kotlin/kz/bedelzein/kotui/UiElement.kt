package kz.bedelzein.kotui

import android.content.Context

interface UiElement<T> {

    suspend fun build(context: Context): T

    interface Attr<T> {
        suspend fun setValue(uiElement: T)
    }

    interface ValueAttr<T, R> : Attr<T> {
        val value: R
    }
}