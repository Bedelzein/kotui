package kz.bedelzein.kotui

internal object LazyIds {

    private val map = mutableMapOf<String, Int>()

    operator fun get(uiConnectableName: String) = map[uiConnectableName]

    operator fun set(uiConnectableName: String, idValue: Int) {
        map[uiConnectableName] = idValue
    }
}