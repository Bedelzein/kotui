package kz.bedelzein.kotui

import android.content.Context
import android.view.View
import android.view.ViewGroup
import kz.bedelzein.core.kotlin.claim
import kz.bedelzein.core.kotlin.claimNotNull
import kotlin.reflect.KClass
import kotlin.reflect.KTypeParameter
import kotlin.reflect.KTypeProjection
import kotlin.reflect.full.createType

@Deprecated("Try to avoid using this object")
object Hardcode {

    private const val LAYOUT_PARAMS_GENERIC_NAME = "LayoutParams"

    fun handleException(e: Throwable) {
        e.printStackTrace()
        throw e
    }

    fun createView(uiConnectable: UiConnectable<*>, context: Context): View {
        val viewClass: KClass<*> = uiConnectable.viewClass ?: let {
            val generic = uiConnectable::class.supertypes
                .first { it.arguments.isNotEmpty() }
                .arguments.first()
            getClassFromGeneric(generic)
        }
        val constructor = viewClass.constructors
            .filter { it.parameters.size == 1 }
            .firstOrNull { it.parameters.first().type.classifier == Context::class }
            ?: throw MissingViewConstructorException(viewClass)
        return claim(constructor.call(context))
    }

    fun <LP : ViewGroup.LayoutParams> createLayoutParams(
        uiComposite: UiComposite<*, *>,
        width: Int,
        height: Int
    ): LP {
        val uiCompositeClass = uiComposite::class
        val layoutParamsClass: KClass<*> = if (uiCompositeClass.typeParameters.isNotEmpty()) {
            val layoutParamsType = uiCompositeClass
                .typeParameters
                .first { it.name.contains(LAYOUT_PARAMS_GENERIC_NAME) }
            val classifier = layoutParamsType.upperBounds.first().classifier
            claimNotNull(classifier)
            claim(classifier)
        } else {
            val generic = uiCompositeClass.supertypes
                .filter { it.arguments.isNotEmpty() }
                .map { it.arguments }
                .flatten()
                .first { getClassFromGeneric(it).isLayoutParams() }
            getClassFromGeneric(generic)
        }
        val layoutParams = layoutParamsClass.constructors
            .filter { it.parameters.size == 2 }
            .first {
                it.parameters.all { parameter ->
                    parameter.type.classifier == Int::class.createType().classifier
                }
            }
            .call(width, height)
        return layoutParams as LP
    }

    private fun getClassFromGeneric(
        generic: KTypeProjection
    ) : KClass<*> = when (val classifier = generic.type?.classifier) {
        is KClass<*> -> classifier
        is KTypeParameter -> claim(claimNotNull(classifier.upperBounds.first().classifier))
        else -> throw IllegalArgumentException("classifier field should be either KClass or KTypeParameter")
    }

    private fun KClass<*>.isLayoutParams(): Boolean =
        qualifiedName?.contains(LAYOUT_PARAMS_GENERIC_NAME) == true
}