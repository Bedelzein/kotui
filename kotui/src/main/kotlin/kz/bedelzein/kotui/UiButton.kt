package kz.bedelzein.kotui

import android.widget.TextView
import androidx.annotation.StringRes
import com.google.android.material.button.MaterialButton

class UiButton : UiConnectable<MaterialButton>()

abstract class Text<V : TextView, T> : UiElement.ValueAttr<V, T> {

    companion object {
        operator fun <V : TextView> invoke(@StringRes value: Int) = Resource<V>(value)
        operator fun <V : TextView> invoke(value: String) = Value<V>(value)
    }

    class Resource<V : TextView>(
        @StringRes override val value: Int
    ) : Text<V, Int>() {
        override suspend fun setValue(uiElement: V) = uiElement.setText(value)
    }

    class Value<V : TextView>(
        override val value: String
    ) : Text<V, String>() {
        override suspend fun setValue(uiElement: V) = uiElement.setText(value)
    }
}