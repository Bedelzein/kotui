package kz.bedelzein.kotui

import kotlin.reflect.KClass

internal class MissingViewConstructorException(
    viewClass: KClass<*>
) : IllegalArgumentException(
    (viewClass.qualifiedName ?: "Subclass of View") +
            " must have a constructor with one argument of type Context"
)