package kz.bedelzein.kotui

import android.view.ViewGroup
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner

const val WRAP_CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT
const val MATCH_PARENT = ViewGroup.LayoutParams.MATCH_PARENT

fun interface OnCreateObserver : DefaultLifecycleObserver {

    fun doOnCreate()

    override fun onCreate(owner: LifecycleOwner) = doOnCreate()
}

fun interface OnDestroyObserver : DefaultLifecycleObserver {

    fun doOnDestroy()

    override fun onDestroy(owner: LifecycleOwner) = doOnDestroy()
}

operator fun Lifecycle.plusAssign(observer: LifecycleObserver) = addObserver(observer)