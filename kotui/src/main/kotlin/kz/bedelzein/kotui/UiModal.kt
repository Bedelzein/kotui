package kz.bedelzein.kotui

import android.app.Dialog

interface UiModal<D : Dialog> : UiElement<D>