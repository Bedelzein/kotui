package kz.bedelzein.kotui

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import kz.bedelzein.core.kotlin.claim
import kotlin.reflect.KClass

private const val VIOLATION_SINGLE_SOURCE_OF_TRUE =
    "You shouldn't set UiConnectable's field more than once due to SSOT (Single source of true)"

private const val WRONG_LAYOUT_PARAMS_SETUP_ORDER =
    "Layout params must be set last"

private const val UI_CONNECTABLE_PATH_SEPARATOR = ":"

open class UiConnectable<V : View> @PublishedApi internal constructor() : UiElement<V> {

    // TODO move to uiElement
    private var viewAttrs: Set<UiElement.Attr<V>>? = null

    /**
     * Required for [LazyIds] to get cached id value by UiConnectable's path
     * */
    protected var path: String? = null

    /**
     * Required for creating view by constructor of kotlin class
     * */
    @PublishedApi
    internal var viewClass: KClass<V>? = null

    /**
     * Required for initializing [UiConnectable]
     * @see [UiConnectable.invoke]
     *
     * Required invocation order: [init] -> [id] -> [placing] -> [viewAttrs]
     * [id] need to be invoked before [placing] due to [UiConstraint] setup.
     *
     * TODO remember, why type is not like this: (T.() -> Unit)?
     * */
    @PublishedApi
    internal var init: (() -> Unit)? = null

    /**
     * Wrapper of [ViewGroup.LayoutParams]
     * */
    internal var placing: Placing = Placing.NotSet

    var id: Id = Id.Undefined

    val isIdUndefined = id is Id.Undefined

    @CallSuper
    override suspend fun build(context: Context): V {
        init?.invoke()
        val view = createView(context)
        id.apply(view)
        placing.execute(view)
        viewAttrs?.forEach {
            val result = runCatching { it.setValue(view) }
            result.exceptionOrNull()?.let(Hardcode::handleException)
        }
        return view
    }

    /**
     * Required for [LazyIds]
     * */
    internal fun getPath(parentPath: String, position: Int): String {
        path?.let { return it }
        val newPath = parentPath + UI_CONNECTABLE_PATH_SEPARATOR + position
        path = newPath
        return newPath
    }

    /**
     * Setting [View] or its derivation's attributes
     * */
    @PublishedApi
    internal fun setAttrs(attrs: Set<UiElement.Attr<V>>) {
        require(viewAttrs == null) { VIOLATION_SINGLE_SOURCE_OF_TRUE }
        requirePlacingNotSetBefore()
        viewAttrs = attrs
    }

    internal fun requirePlacingNotSetBefore() {
        require(placing == Placing.NotSet) { WRONG_LAYOUT_PARAMS_SETUP_ORDER }
    }

    private fun createView(context: Context): V = Hardcode.createView(this, context) as? V
        ?: throw WrongGenericProvidedException(this)

    companion object {
        inline fun <reified V : View> custom() = UiConnectable<V>().also { it.viewClass = V::class }

        /**
         * Sets attrs of [View] or its derivation
         * Operator function is used in case of preventing multiple function renaming
         * @see [UiConnectable.setAttrs]
         * */
        inline operator fun <reified V : View> get(
            vararg attrs: UiElement.Attr<V>
        ): UiConnectable<V> {
            val uiConnectable = UiConnectable<V>()
            uiConnectable.viewClass = V::class
            uiConnectable.setAttrs(attrs.toSet())
            return uiConnectable
        }
    }

    private class WrongGenericProvidedException(
        uiConnectable: UiConnectable<*>
    ) : IllegalArgumentException(
        "Wrong generic provided for " +
                (uiConnectable::class.qualifiedName ?: "subclass of UiConnectable")
    )

}

sealed class Placing {

    open fun execute(view: View) = Unit

    object NotSet : Placing()

    object Trunk : Placing() {
        override fun execute(view: View) {
            view.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
        }
    }

    class Continuation<LP : ViewGroup.LayoutParams>(
        private val value: LP,
        private val init: (LP.() -> Unit)? = null
    ) : Placing() {

        override fun execute(view: View) {
            view.layoutParams = value
            init?.invoke(value)
        }
    }
}

sealed class AggregationState(val width: Int, val height: Int) {

    fun getParams() = Params(width, height)

    data class Params(val width: Int, val height: Int)

    class Custom(width: Int, height: Int) : AggregationState(width, height) {
        operator fun not() = Custom(this.height, this.width)
    }
}

object Earth : AggregationState(WRAP_CONTENT, WRAP_CONTENT)

object Water : AggregationState(MATCH_PARENT, WRAP_CONTENT)

object Air : AggregationState(MATCH_PARENT, MATCH_PARENT)

object Fire : AggregationState(WRAP_CONTENT, MATCH_PARENT)

sealed class Id {

    abstract fun apply(view: View)

    fun requireDefinedValue() = claim<Defined>(this).value

    companion object {
        operator fun invoke(value: Int) = Defined(value)
    }

    object Undefined : Id() {
        override fun apply(view: View) = view.setId(View.NO_ID)
    }

    data class Defined(
        val value: Int
    ) : Id() {
        override fun apply(view: View) = view.setId(value)
    }
}

/**
 * Setup attrs for [View]
 * */
operator fun <V : View, T : UiConnectable<V>> T.get(
    vararg attrs: UiElement.Attr<V>
): T {
    setAttrs(attrs.toSet())
    return this
}

/**
 * Setup [UiConnectable]
 * @see [UiConnectable.init]
 * */
operator fun <T : UiConnectable<*>> T.invoke(block: T.() -> Unit): T {
    require(init == null) { VIOLATION_SINGLE_SOURCE_OF_TRUE }
    requirePlacingNotSetBefore()
    init = { this.block() }
    return this
}

@Suppress("FunctionMinLength")
infix fun Int.x(height: Int): AggregationState.Custom {
    val width = this
    return AggregationState.Custom(width, height)
}

val Int.earth get(): AggregationState.Custom {
    val width = this
    return width x WRAP_CONTENT
}

val Int.water get(): AggregationState.Custom {
    val height = this
    return MATCH_PARENT x height
}

val Int.fire get(): AggregationState.Custom {
    val width = this
    return width x MATCH_PARENT
}

class Click<V : View>(
    override val value: FlowCollector<Unit>
) : UiElement.ValueAttr<V, FlowCollector<Unit>> {
    override suspend fun setValue(uiElement: V) {
        uiElement.setOnClickListener { runBlocking { value.emit(Unit) } }
    }
}

class ClickListener<V : View>(
    override val value: View.OnClickListener
) : UiElement.ValueAttr<V, View.OnClickListener> {
    override suspend fun setValue(uiElement: V) {
        uiElement.setOnClickListener(value)
    }
}