package kz.bedelzein.kotui

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.activity.ComponentActivity
import androidx.lifecycle.lifecycleScope
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

class ScreenUi<T : UiConnectable<*>>(private val uiConnectable: T) {

    init {
        uiConnectable.placing = Placing.Trunk
    }

    suspend fun grow(context: Context) = uiConnectable.build(context)

    suspend fun applyTo(activity: Activity) {
        uiConnectable.placing = Placing.Trunk
        val name = uiConnectable.getPath(requireNotNull(activity::class.qualifiedName), 0)
        val idValue = View.generateViewId()
        val id = Id.Defined(idValue)
        uiConnectable.id = id
        LazyIds[name] = idValue
        val view = uiConnectable.build(activity)
        activity.setContentView(view)
    }

    class Delegate<T : UiConnectable<*>>(
        private val activity: ComponentActivity,
        uiTrunkClass: KClass<T>,
        initUiTrunk: T.() -> Unit
    ) : ReadOnlyProperty<ComponentActivity, ScreenUi<*>> {

        private var screenUi: ScreenUi<*>? = null

        init {
            activity.lifecycleScope.launchWhenCreated {
                val uiScreen = ScreenUi(createScreenUi(uiTrunkClass, initUiTrunk))
                this@Delegate.screenUi = uiScreen
                val view = uiScreen.grow(activity)
                activity.setContentView(view)
            }
            activity.lifecycle += OnDestroyObserver {
                // TODO dispose uiTree
                screenUi = null
            }
        }

        override fun getValue(
            thisRef: ComponentActivity,
            property: KProperty<*>
        ): ScreenUi<*> = requireNotNull(screenUi) {
            """
                Should not attempt to get uiTree when Activity is not created yet or destroyed.
                Current state: ${activity.lifecycle.currentState.name}
            """.trimIndent()
        }

        private fun createScreenUi(uiTrunkClass: KClass<T>, init: T.() -> Unit): T {
            val uiTrunk = uiTrunkClass.constructors.first().call()
            uiTrunk.placing = Placing.Trunk
            val name = uiTrunk.getPath(requireNotNull(activity::class.qualifiedName), 0)
            val idValue = View.generateViewId()
            val id = Id.Defined(idValue)
            uiTrunk.id = id
            LazyIds[name] = idValue
            uiTrunk.init()
            return uiTrunk
        }
    }
}