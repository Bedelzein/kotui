package kz.bedelzein.kotui

import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.runBlocking
import kz.bedelzein.kotui.test.R

class CheckKotuiActivity : AppCompatActivity(), KotuiActivity {

    fun checkUiConstraint() {
        val uiConstraint = UiConstraint.Itself {
            UiConnectable[Text("Hello, World!")] {
                id = Id(R.id.hello_world_text_view)
                startTo { parentStart }
                topTo { parentTop }
                endTo { parentEnd }
                bottomTo { parentBottom }
            }(Earth)

            lateinit var bButton: UiButton
            lateinit var cButton: UiButton
            lateinit var dButton: UiButton

            val aButton = UiButton()[Text.Value("a")] {
                startTo { parentStart }
                endTo { bButton.start }
                bottomTo { parentBottom + 8.dip }
            }(70.dip.earth)

            bButton = UiButton()[Text.Value("b")] {
                startTo { aButton.end }
                topTo { aButton.top }
                endTo { cButton.start }
                bottomTo { aButton.bottom }
            }(70.dip.earth)

            cButton = UiButton()[Text.Value("c")] {
                startTo { bButton.end }
                topTo { aButton.top }
                endTo { dButton.start }
                bottomTo { aButton.bottom }
            }(70.dip.earth)

            dButton = UiButton()[Text.Value("d")] {
                startTo { cButton.end }
                topTo { aButton.top }
                endTo { parentEnd }
                bottomTo { aButton.bottom }
            }(70.dip.earth)
        }
        runBlocking { ScreenUi(uiConstraint).applyTo(this@CheckKotuiActivity) }
    }

    fun checkUiList() {
        class MockItem(val value: Int) : UiList.Item {
            override fun isSameAs(item: UiList.Item): Boolean =
                item is MockItem && item.value == value

            override fun areContentsSameWith(item: UiList.Item): Boolean =
                item is MockItem && item.value == value
        }

        val uiList = UiList<MockItem>()[Linear] {
            uiItem<MockItem>(Water) { provider ->
                UiButton()[Text(provider.defaultItem.value.toString())]
            }
        }
        uiList.submitItems((1..10).map { MockItem(it) })
        runBlocking { ScreenUi(uiList).applyTo(this@CheckKotuiActivity) }
    }
}