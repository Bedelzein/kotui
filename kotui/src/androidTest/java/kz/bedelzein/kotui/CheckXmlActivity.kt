package kz.bedelzein.kotui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kz.bedelzein.core.android.toast
import kz.bedelzein.core.kotlin.claim
import kz.bedelzein.kotui.test.R

class CheckXmlActivity : AppCompatActivity() {

    fun checkUiConstraint() {
        setContentView(R.layout.screen_ui_constraint)
    }

    fun checkUiList() {
        setContentView(R.layout.screen_ui_list)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        val numbers = (1..10).toList()
        val numberDiffUtilCallback = object : DiffUtil.ItemCallback<Int>() {
            override fun areItemsTheSame(oldItem: Int, newItem: Int) = oldItem == newItem
            override fun areContentsTheSame(oldItem: Int, newItem: Int) =
                areItemsTheSame(oldItem, newItem)
        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(item: Int) {
                claim<Button>(itemView).text = item.toString()
            }
        }
        recyclerView.adapter = object : ListAdapter<Int, ViewHolder>(numberDiffUtilCallback) {
            override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val view = inflater.inflate(R.layout.item, parent, false)
                val viewHolder = ViewHolder(view)
                view.setOnClickListener {
                    val adapterPosition = viewHolder.adapterPosition
                        .takeIf { it != RecyclerView.NO_POSITION }
                        ?: return@setOnClickListener
                    val item = getItem(adapterPosition)
                    toast("$item clicked!")
                }
                return viewHolder
            }

            override fun onBindViewHolder(holder: ViewHolder, position: Int) =
                holder.bind(getItem(position))

            override fun getItemViewType(position: Int) = position
        }.also { it.submitList(numbers) }
    }
}