package kz.bedelzein.kotui

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.runner.screenshot.Screenshot
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.io.FileOutputStream

private const val IMAGE_COMPRESS_QUALITY_MAX = 100

@RunWith(AndroidJUnit4::class)
class TestingScreenTest {

    @Test
    fun checkIfUiConstraintMatchesToXml() {
        lateinit var kotuiActivity: CheckKotuiActivity
        ActivityScenario.launch(CheckKotuiActivity::class.java).onActivity {
            it.checkUiConstraint()
            kotuiActivity = it
        }
        val kotuiScreenshot = kotuiActivity.screenshot()
        lateinit var xmlActivity: CheckXmlActivity
        ActivityScenario.launch(CheckXmlActivity::class.java).onActivity {
            it.checkUiConstraint()
            xmlActivity = it
        }
        val xmlScreenshot = xmlActivity.screenshot()
        assert(kotuiScreenshot.bitmap.sameAs(xmlScreenshot.bitmap))
    }

    @Test
    fun checkIfUiListMatchesToXml() {
        lateinit var kotuiActivity: CheckKotuiActivity
        ActivityScenario.launch(CheckKotuiActivity::class.java).onActivity {
            it.checkUiList()
            kotuiActivity = it
        }
        val kotuiScreenshot = kotuiActivity.screenshot()
        lateinit var xmlActivity: CheckXmlActivity
        ActivityScenario.launch(CheckXmlActivity::class.java).onActivity {
            it.checkUiList()
            xmlActivity = it
        }
        val xmlScreenshot = xmlActivity.screenshot()
        assert(kotuiScreenshot.bitmap.sameAs(xmlScreenshot.bitmap))
    }

    private fun Activity.screenshot() = Screenshot.capture(this)

    /**
     * TODO save screenshots of test failed
     * https://medium.com/stepstone-tech/how-to-capture-screenshots-for-failed-ui-tests-9927eea6e1e4
     * */
    @Suppress("unused")
    private fun saveScreenshot(context: Context, name: String, bitmap: Bitmap) {
        val dir = context.filesDir
        val screenshotFile = File("$dir/$name.jpg")
        FileOutputStream(screenshotFile).use {
            bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESS_QUALITY_MAX, it)
            it.flush()
        }
    }
}